<Row className="mb-2">
  <Form.Group as={Col} sm="12" controlId="validationCustom01">
    <InputGroup hasValidation>
      <InputGroup.Text id="inputGroupPrepend">Test ID</InputGroup.Text>
      <Form.Control
        type="text"
        placeholder="22311"
        aria-describedby="inputGroupPrepend"
        required
      />
      <Form.Control.Feedback type="invalid">
        Please provide Test ID
      </Form.Control.Feedback>
    </InputGroup>
  </Form.Group>
</Row>;
