import React, { Component } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

class Title extends React.Component {
  render() {
    return (
      <Container>
        <Row></Row>
        <Row>
          <Col sm={12}>
            <h4 style={{ marginTop: "8px" }}>Duster Cleaning Fine Dust</h4>
          </Col>
        </Row>
        <Row
          className="align-items-center"
          style={{ borderInlineColor: "black" }}
        >
          <Col sm={"auto"}>TM-100658/01</Col>
          <Col sm={10}>
            Test Summary{" "}
            <Col sm={10} style={{ fontSize: "12px", color: "grey" }}>
              Test summary from all samples and trials
            </Col>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Title;
