import React, { Component } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Button from "react-bootstrap/Button";

const Forms = props => {
  return (
    <Container>
      <Row>
        <Col sm={12} style={{ marginTop: "25px" }}>
          <b>Product Details</b>
        </Col>
      </Row>

      <Form
        noValidate
        validated={props.validated}
        onSubmit={props.handleSubmit}
      >
        <Form.Group as={Row} className="mb-2" controlId="validationCustom01">
          <Form.Label column md={12} lg={2}>
            Test Request Number
          </Form.Label>
          <Col md={12} lg={10}>
            <Form.Control
              name="trNumber"
              type="text"
              placeholder="TR1234"
              onChange={props.handleChange.bind(this)}
              required
            />
            <Form.Control.Feedback type="invalid">
              Please provide Test Request Number
            </Form.Control.Feedback>
          </Col>
        </Form.Group>

        <Form.Group as={Row} className="mb-2" controlId="validationCustom01">
          <Form.Label column md={12} lg={2}>
            Test ID
          </Form.Label>
          <Col md={12} lg={10}>
            <Form.Control
              name="testId"
              type="text"
              placeholder="22311"
              onChange={props.handleChange.bind(this)}
              required
            />
            <Form.Control.Feedback type="invalid">
              Please provide Test ID
            </Form.Control.Feedback>
          </Col>
        </Form.Group>

        <Form.Group as={Row} className="mb-2" controlId="validationCustom01">
          <Form.Label column md={12} lg={2}>
            Clause
          </Form.Label>
          <Col md={12} lg={10}>
            <Form.Control
              name="Clause"
              type="text"
              placeholder="N/A"
              required
              onChange={props.handleChange.bind(this)}
            />
            <Form.Control.Feedback type="invalid">
              Please provide Clause
            </Form.Control.Feedback>
          </Col>
        </Form.Group>

        <Row className="mb-2">
          <Col md={12} lg={6}>
            <Form.Group as={Row} controlId="validationCustom01">
              <Form.Label column sm={12} lg={4}>
                Build Phase
              </Form.Label>
              <Col md={12} lg={8}>
                <Form.Control
                  name="buildPhase"
                  required
                  type="text"
                  placeholder="N/A"
                  onChange={props.handleChange.bind(this)}
                />
                <Form.Control.Feedback>
                  Please Provide Build Phase
                </Form.Control.Feedback>
              </Col>
            </Form.Group>
          </Col>
          <Col md={12} lg={6}>
            <Form.Group as={Row} controlId="validationCustom02">
              <Form.Label column sm={12} lg={4}>
                Ambient Temperature
              </Form.Label>
              <Col md={12} lg={8}>
                <Form.Control
                  name="ambientTemperature"
                  required
                  type="number"
                  placeholder="22"
                  onChange={props.handleChange.bind(this)}
                />
                <Form.Control.Feedback>
                  Please Provide Ambient Temperature
                </Form.Control.Feedback>
              </Col>
            </Form.Group>
          </Col>
        </Row>
        <br />
        <Button type="submit">Submit</Button>
      </Form>
    </Container>
  );
};

export default Forms;
