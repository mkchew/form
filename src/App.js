import React, { Component } from "react";
import Title from "./components/Title";
import Forms from "./components/Forms";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      form: [],
      validated: false
    };
  }

  handleSubmit = event => {
    console.log("submit");
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    //event.preventDefault();
    this.setState({ validated: true });
  };

  handleChange = event => {
    let fieldName = event.target.name;
    let fleldVal = event.target.value;
    this.setState({ form: { ...this.state.form, [fieldName]: fleldVal } });
  };

  render() {
    return (
      <div>
        <Title />
        <Forms
          validated={this.state.validated}
          handleSubmit={this.handleSubmit}
          handleChange={this.handleChange}
        />
      </div>
    );
  }
}

export default App;
